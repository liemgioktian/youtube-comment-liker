var fs        = require('fs')
var Nightmare = require('nightmare')
var videos    = []
var users     = []
var tester    = {}
var nmopt     = {
  waitTimeout: 150000,
  typeInterval: 20,
  show: false, 
  // openDevTools: true,
  webPreferences: {
    images: true, 
    partition: 'nopersist'
  },
  switches: {
    'proxy-server': '',
  }
}
var done = false

/*
 - read input
 - retrieve initial thumbs

 - like all videos
 - validate all videos, goto 3
*/
return readInput('video_url.txt')
.then(vids => {
  for (var v in vids) {
    if (vids[v].length < 1) continue
    var vpart = vids[v].split(';')
    videos.push({
      toadd: parseInt(vpart[0]),
      url: vpart[1].replace('\r', '')
    })
  }
  return readInput('user_list.txt')
})
.then(us => {
  for (var u in us) {
    if (us[u].length < 1) continue
    var upart = us[u].split(';')
    if (0 == u) tester = {
      uname : upart[0],
      pwd   : upart[1],
      proxy : upart[2].replace('\r', '')
    }
    else users.push({
      uname : upart[0],
      pwd   : upart[1],
      proxy : upart[2].replace('\r', '')
    })
  }
  return true
})
.then(() => {
  //console.log(videos)
  nmopt.switches['proxy-server'] = tester.proxy
  var nightmare = new Nightmare(nmopt)
  nightmare
  .goto('https://www.google.com/')
  .evaluate(() => {
    document.querySelector('[href^="https://accounts.google.com/ServiceLogin?"]').click()
  })
  .wait('#Email')
  .type('#Email', tester.uname)
  .click('#next')
  .wait('#Passwd')
  .type('#Passwd', tester.pwd)
  .click('#signIn')
  .wait('#footer')
  .then(() => {
    initiation (nightmare, 0, main)
  })
})

function main () {
  console.log('main')
  if (done) return true
  thumbs_up (0, 0, () => {
    nmopt.switches['proxy-server'] = tester.proxy
    var nightmare = new Nightmare(nmopt)
    nightmare
    .goto('https://www.google.com/')
    .evaluate(() => {
      document.querySelector('[href^="https://accounts.google.com/ServiceLogin?"]').click()
    })
    .wait('#Email')
    .type('#Email', tester.uname)
    .click('#next')
    .wait('#Passwd')
    .type('#Passwd', tester.pwd)
    .click('#signIn')
    .wait('#footer')
    .then(() => {
      validation(nightmare, 0, main)
    })
  })
}

function initiation (nightmare, v, cb) {
  console.log('initiation')
  nightmare
  .goto(videos[v].url)
  .evaluate(() => {
    var count = document.querySelector('.comment-renderer-linked-comment')
      .parentElement.parentElement
    var off = count.querySelector('.comment-renderer-like-count').innerHTML
    var on = count.querySelector('.comment-renderer-like-count.on').innerHTML
    return parseInt(off) > parseInt(on) ? off : on
  })
  .then(thumbs => {
    videos[v].initial = parseInt(thumbs)
    if (videos[v + 1]) initiation (nightmare, v + 1, cb)
    else {
      nightmare.end().then()
      cb && cb()
    }
  })
  .catch(e => {
    console.error(e)
  })
}

function validation (nightmare, v, cb) {
  console.log('validation')
  nightmare
  .goto(videos[v].url)
  .evaluate(() => {
    var count = document.querySelector('.comment-renderer-linked-comment')
      .parentElement.parentElement
    var off = count.querySelector('.comment-renderer-like-count').innerHTML
    var on = count.querySelector('.comment-renderer-like-count.on').innerHTML
    return parseInt(off) > parseInt(on) ? off : on
  })
  .then(thumbs => {
    videos[v]._final = parseInt(thumbs)
    videos[v].toadd = videos[v].initial + videos[v].toadd - videos[v]._final

    if (0 <= videos[v].toadd) {
      if (1 <= videos.length) done = true
      videos = videos.splice(v, 1)
    }

    if (videos[v + 1]) validation (nightmare, v + 1, cb)
    else {
      nightmare.end().then()
      cb && cb()
    }
  })
}

function thumbs_up (v, u, cb) {
  console.log('thumbs_up', v, u)
  if (undefined === videos[v]) {
    cb ()
    return false
  }

  if (videos[v].added && videos[v].added >= videos[v].toadd) {
    thumbs_up(v+1, 0, cb)
    return false
  }

  if (!users[u]) {
    console.log('user telah habis untuk video ke ' + parseInt(v+1))
    thumbs_up(v+1, 0, cb)
    return false
  }

  if (undefined === videos[v].last_user && u > 0) {
    thumbs_up(v, 0, cb)
    return false
  }

  if (videos[v].last_user && u <= videos[v].last_user) {
    thumbs_up(v, u + 1, cb)
    return false
  }

  nmopt.switches['proxy-server'] = users[u].proxy
  var nightmare = Nightmare(nmopt)
  return nightmare
  .goto('https://www.google.com/')
  .evaluate(() => {
    document.querySelector('[href^="https://accounts.google.com/ServiceLogin?"]').click()
  })
  .wait('#Email')
  .type('#Email', users[u].uname)
  .click('#next')
  .wait('#Passwd')
  .type('#Passwd', users[u].pwd)
  .click('#signIn')
  .wait('#footer')
  .goto(videos[v].url)
  .wait(() => {
    var el = document.querySelector('.comment-renderer-linked-comment')
      .parentElement.parentElement.querySelector('[aria-label="Like"]')
    if (null === el) return true

    var elemTop = el.getBoundingClientRect().top
    var elemBottom = el.getBoundingClientRect().bottom
    var isVisible = (elemTop >= 0) && (elemBottom <= window.innerHeight)
    return isVisible === true
  })
  .evaluate(() => {
    var el = document.querySelector('.comment-renderer-linked-comment')
    .parentElement.parentElement.querySelector('[aria-label="Like"]')
    if (null === el) return true
    el.click()
  })
  .end()
  .then(liked => {
    if (!liked) {
      console.log('added')
      videos[v].added = videos[v].added ? videos[v].added+1 : 1
    } else console.log(users[u].uname + ' sudah me-like video ke ' + parseInt(v+1) + ' sebelumnya')
    videos[v].last_user = u
    thumbs_up (v, u + 1, cb)
  })
  .catch(e => {
    console.error(users[u].uname + ' ' + e)
    thumbs_up (v, u + 1, cb)
  })
}

function readInput (filename) {
  return new Promise ((res,rej) => {
    fs.readFile(filename, 'utf8', function(err, data) {
      res (data.split('\n'))
    })
  })
}